#include <stdio.h>
#include <stdlib.h>
#include "utils.cuh"
#include <cuda_runtime.h>ca

#define THREADS 16;


using namespace std;




/*
__global__ void matMultiplicationCuda(int **amatrix, int**bmatrix,
                                      int**cmatrix, int m, int k, int n){
    __shared__ int atmp[THREADS][THREADS], btmp[THREADS][THREADS];
    int row = threadIdx.x + blockDim.x * blockIdx.x;
    int col = threadIdx.y + blockDim.y * blockIdx.y;

    float sum = 0.0;

    for(int i =0; < ceil(k,(float)THREADS); ++i){
        if((m<row) && (i*THREADS))
    }
}
*/



__global__ void haar_cuda(float **matrix, float **filter, float **out, int m, int n, int dimfilter){
    int row = threadIdx.x + blockIdx.x*blockDim.x;
    int col = threadIdx.y + blockIdx.y*blockDim.y;

    if(row<m && col<n){
        //printf("%f\n",matrix[row][col]);
        if(col % 8 == 0){
          printf("%d\n",  );
            printf("%d\t%d\n",row,col);
            multMatrixRow(matrix,filter,out,row, row+1,col,col+dimfilter,dimfilter);
            __syncthreads();
        }
        //print_matrix(out,m,n);
        if(row % 8 == 0){
            multMatrixCol(matrix,filter,out,row, row+dimfilter,col, col+1,dimfilter);
            __syncthreads();
        }
        //printf("salida %d\t%d\t%f\n", row, col, out[row][col]);

    }
}





int main(){
    CImg<unsigned char> image("minion256.jpg");

    int row = image.height(), col = image.width();
    size_t  total_size= sizeof(float)*image.width()*image.height();
    float **rhmatrix, **rdmatrix, **rtmatrix; // R
    float **ghmatrix, **gdmatrix, **gtmatrix; // G
    float **bhmatrix, **bdmatrix, **btmatrix; // B
    float **fhmatrix, **fdmatrix, **ftmatrix;
    float **rhomatrix, **rdomatrix, **rtomatrix; // R out
    float **ghomatrix, **gdomatrix, **gtomatrix; // G out
    float **bhomatrix, **bdomatrix, **btomatrix; // B out

    printf("casa0");

    createDeviceMatrix(rhmatrix, rdmatrix, rtmatrix, row, col);
    createDeviceMatrix(ghmatrix, gdmatrix, gtmatrix, row, col);
    createDeviceMatrix(bhmatrix, bdmatrix, btmatrix, row, col);

    createDeviceMatrix(rhomatrix, rdomatrix, rtomatrix, row, col);
    createDeviceMatrix(ghomatrix, gdomatrix, gtomatrix, row, col);
    createDeviceMatrix(bhomatrix, bdomatrix, btomatrix, row, col);
    // prueba
    createMatrixs(fhmatrix,fdmatrix,ftmatrix,8,8);
    // llenando filtro
    set8matrix(fhmatrix);
    //llenando matriz
    //fill_matrix(rhmatrix,8,8);
    //setexample(rhmatrix);
    imageToMatrixs(image, rhmatrix, ghmatrix, bhmatrix);
    //print_matrix(rhmatrix,8,8);
    printf("imprmiendo filtro");
    print_matrix(fhmatrix,8,8);

    //multMatrix(rhmatrix, fhmatrix, ghmatrix,0,1,0,8,8);
    //print_matrix(ghmatrix,8,8);
    //dim3 blocksPerGrid( (row-1)/8+1, (col-1)/8+1);
    dim3 blocksPerGrid( 1, 1);
    dim3 ThreadsPerBlock(8, 8);

    cudaMemcpy(rtmatrix[0], rhmatrix[0], total_size,cudaMemcpyHostToDevice);
    cudaMemcpy(ftmatrix[0], fhmatrix[0], sizeof(int)*8*8, cudaMemcpyHostToDevice);


    haar_cuda<<<blocksPerGrid, ThreadsPerBlock>>>(rdmatrix, fdmatrix, rdomatrix, row, col, 8);
    cudaDeviceSynchronize();

    cudaMemcpy(rhomatrix[0], rtomatrix[0], total_size, cudaMemcpyDeviceToHost);
    print_matrix(rhomatrix,row,col);

    cudaFree(rdmatrix);
    cudaFree(fdmatrix);

    printf("hola mundo\n");
    return 0;
}
