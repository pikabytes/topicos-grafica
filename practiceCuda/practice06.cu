#include <stdio.h>
#include <stdlib.h>
#include <stdlib.h>
#include <time.h>


#define N 10
#define THREADS_PER_BLOCK 30
#define RADIUS 3
#define BLOCK_SIZE 30

__global__ void stencil_1d(int *in, int *out){
	__shared__ int temp[BLOCK_SIZE + 2*RADIUS];
	int gindex = threadIdx.x + blockIdx.x*blockDim.x;
	int lindex = threadIdx.x + RADIUS;

	printf("%d\t%d\n",gindex,lindex );
	//leemos los elementos en memoria compartida 
	temp[lindex] = in[gindex];
	if(threadIdx.x < RADIUS){
		temp[lindex - RADIUS] = in[gindex - RADIUS];
		temp[lindex + BLOCK_SIZE] = in[gindex + BLOCK_SIZE];	
	}
	__syncthreads();
	int result = 0;
	for(int offset = -RADIUS; offset <= RADIUS; offset++)
		result += temp[lindex + offset];

	out[gindex] = result;
}


void random_ints(int *a, int n){
	for (int i = 0; i < n; ++i){
		a[i] = rand() % 10 +1;
	}
}

void fill_zero(int *a, int n){
	for (int i = 0; i < n; ++i){
		a[i] = 0;
	}
}


void print_ints(int *a, int n){
	for (int i = 0; i < n; ++i)
		printf("%d ",a[i] );
	printf("\n");
	printf("\n");
}

void sumRadio(int *a,int *b, int pos, int radio, int tam){
	for (int i = (pos-radio); i <= (pos+radio); ++i){
		if( i>=0 || i<tam){
			b[pos] += a[i];
		}
	}
}


void serial(int *in, int *out, int n, int radio){
	for (int i = 0; i < n; ++i){
		sumRadio(in, out, i, radio, n);
	}

}

int main(void){
	int *a, *b, *c;
	int *d_a, *d_b;
	int size = N*sizeof(int);

	//cudaMalloc((void**)&d_a, (N + 2*RADIUS)*sizeof(int));	
	cudaMalloc((void**)&d_a, size);	
	cudaMalloc((void**)&d_b, size);

	a = (int*)malloc(size);	random_ints(a,N);
	b = (int*)malloc(size);
	c = (int*)malloc(size); fill_zero(c,N);
	cudaMemcpy(d_a, a, size, cudaMemcpyHostToDevice);


	float dt_cuda, dt_serial;
	cudaEvent_t begin, end;
	cudaEventCreate(&begin);
	cudaEventCreate(&end);

	cudaEventRecord(begin, 0);
	//cudaThreadSynchronize();
	//clock_t begin = clock();
	stencil_1d<<<(N + BLOCK_SIZE - 1)/BLOCK_SIZE, BLOCK_SIZE>>>(d_a, d_b);
	cudaThreadSynchronize();
	//clock_t end = clock();

	//dt_cuda = (double)(end - begin)/CLOCKS_PER_SEC;
	cudaEventRecord(end, 0);

	//cudaEventSynchronize(begin);
	//cudaEventSynchronize(end);
	
	cudaEventElapsedTime(&dt_cuda, begin, end);
	

	printf("Vector de entrada\n");
	//print_ints(a,N);

	clock_t tStart, tStop;

	tStart = clock();
	serial(a,c,N,RADIUS);
	tStop = clock();
	dt_serial = (double)(tStop - tStart)/CLOCKS_PER_SEC;
	
	printf("Vector salida serial\n");
	//print_ints(c,N);
	printf("time serial: %lf\n", dt_serial );

	cudaMemcpy(b, d_b, size, cudaMemcpyDeviceToHost);
	
	printf("\n Vector salida Cuda\n");
	//print_ints(b,N);
	printf("time cuda: %lf\n\n", dt_cuda );

	free(a);	free(b);
	cudaFree(d_a),	cudaFree(d_b);

	return 0;
}