#include <stdlib.h>
#include <stdio.h>

#define N 20
#define THREADS_PER_BLOCK 8


__global__ void add(int *a, int *b, int *c){
	int index = threadIdx.x + (blockIdx.x * blockDim.x);
	c[index] = a[index] + b[index]; 
}

__global__ void add2(int *a, int *b, int *c, int n){
        int index = threadIdx.x + (blockIdx.x * blockDim.x);
	if(index < n)
		c[index] = a[index] + b[index];
}


void random_ints(int *a, int n){
	for(int i=0; i<n; ++i)
		a[i] = rand() % n;
}

void print_ints(int *a, int n){
	printf("imprimiendo\n");
	for(int i=0; i<n; ++i)
		printf("%d ", a[i]);
	printf("\n");
}

int main(void){
	int *a, *b, *c;
	int *d_a, *d_b, *d_c;
	int size = N * sizeof(int);
		
	cudaMalloc((void**)&d_a, size);
	cudaMalloc((void**)&d_b, size);
	cudaMalloc((void**)&d_c, size);

	a = (int*)malloc(size);	random_ints(a,N);
	b = (int*)malloc(size);	random_ints(b,N);
	c = (int*)malloc(size); 

	cudaMemcpy(d_a, a, size, cudaMemcpyHostToDevice);
	cudaMemcpy(d_b, b, size, cudaMemcpyHostToDevice);
	
	
	
    //add<<<N/THREADS_PER_BLOCK, THREADS_PER_BLOCK>>>(d_a, d_b, d_c);
    add2<<<(N + THREADS_PER_BLOCK - 1)/THREADS_PER_BLOCK, THREADS_PER_BLOCK>>>(d_a, d_b, d_c,N);
    
	cudaMemcpy(c, d_c, size, cudaMemcpyDeviceToHost);
	
        print_ints(a,N);
        print_ints(b,N);
        print_ints(c,N);

	free(a);	free(b);	free(c);
	cudaFree(d_a);	cudaFree(d_b);	cudaFree(d_c);	
	

	return 0;
}
