#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#define BLOCK_SIZE 12
#define M_A 3
#define NM_AB 4
#define N_B 3

void fill_Mat(double **&matrix, int m, int n){
  for(int i=0; i<m; ++i)
    for(int j=0; j<n; ++j){
      matrix[i][j] = rand()%10+1;
    }
}

void print_Mat(double **matrix, int m, int n){
  for(int i=0; i<m; ++i){
    for(int j=0; j<n; j++)
      printf("%3.f\t", matrix[i][j]);
    printf("\n");
  }
}

void createHostMatrix(double **&a, int m, int n){
  a = new double*[m];
  for(int i=0;i<m;++i)
    a[i] = new double[n];
}


void createDeviceMatrix(double **&h_m, double **&d_m, double **&dd_m,
                  size_t totalsize, int M,int N){
  size_t size_row = sizeof(double*)*M;
  //size_t totalsize = sizeof(double)*M*N;
  //matrix D_H host matrix
  h_m = (double**)malloc(size_row);
  h_m[0] = (double*)malloc(totalsize);
  //matrix D_M device matrix
  cudaMalloc((void**)&d_m, size_row);
  //matrix DD_M
  dd_m = (double**)malloc(size_row);
  cudaMalloc((void**)&dd_m[0], totalsize);
  //linkear las filas a sus respectivas columnas
  for(int i=1; i<M; ++i){
    h_m[i] = h_m[i-1] + N;
    dd_m[i] = dd_m[i-1] + N;
  }
  cudaMemcpy(d_m, dd_m, size_row, cudaMemcpyHostToDevice);
}

void hostMatMult(double **a, double **b, double **c){
  for (size_t i = 0; i < M_A; i++) {
    for (size_t j = 0; j < NM_AB; j++) {
      double sum =0;
      for (size_t k = 0; k < NM_AB; k++) {
        sum += a[i][j]*b[i][j];
      }
      c[i][j] = sum;
    }
  }
}

__global__ void matMult(double **m_a, double **m_b, double **m_c){
  int row = threadIdx.x + blockDim.x * blockIdx.x;
  int col = threadIdx.y + blockDim.y * blockIdx.y;

  if( (row < M_A) && (col < N_B)){
    double sum = 0.0;
    for(int k=0; k< NM_AB; ++k){
      sum += m_a[row][k]*m_b[k][col];
    }
    m_c[row][col] = sum;
  }
}

__global__ void sharedMatMult(double **m_a, double **m_b, double **m_c){
  int row = threadIdx.x + BLOCK_SIZE * blockIdx.x;
  int col = threadIdx.y + BLOCK_SIZE * blockIdx.y;
  int tx = threadIdx.x, ty = threadIdx.y;
  //int bx = blockIdx.x, by = blockIdx.y;
  __shared__ double tmpA[BLOCK_SIZE][BLOCK_SIZE],
              tmpB[BLOCK_SIZE][BLOCK_SIZE];
  double sum = 0.0;


  for (int m=0; m<gridDim.x; ++m) {


    if((row<M_A) && (m*BLOCK_SIZE+tx<NM_AB) )
      tmpA[ty][tx] = m_a[row][m*BLOCK_SIZE+tx];
    else
      tmpA[ty][tx] = 0;
    if((col<N_B) && (m*BLOCK_SIZE+ty<NM_AB) )
      tmpB[ty][tx] = m_b[m*BLOCK_SIZE+ty][col];
    else
      tmpB[ty][tx] = 0;
    __syncthreads();

    for(int k=0; k < BLOCK_SIZE; ++k){
      sum += tmpA[threadIdx.y][k] * tmpB[k][threadIdx.x];
      //printf("%f\t%f\n",tmpA[ty][k], tmpB[k][tx] );
    }
    __syncthreads();
  }
  printf("%f\n",sum );
  if(row < M_A && col < N_B)
    m_c[row][col] = sum;


}

int main(){
  dim3 threadsPerBlock(BLOCK_SIZE, BLOCK_SIZE);
  dim3 blocksPerGrid(((M_A-1)/BLOCK_SIZE)+1,
                      ((N_B-1)/BLOCK_SIZE)+1);
  size_t totalsizeA = sizeof(double)*M_A*NM_AB; // m x n
  size_t totalsizeB = sizeof(double)*NM_AB*N_B; // n x p
  size_t totalsizeC = sizeof(double)*M_A*N_B; // m x p

  //host
  double **a, **b, **c;
  //device
  double **m_1, **m_d1, **m_dd1;
  double **m_2, **m_d2, **m_dd2;
  double **m_3, **m_d3, **m_dd3;

  // HOST MAT MULTIPLICACION
  createHostMatrix(a, M_A, NM_AB);
  createHostMatrix(b, NM_AB, N_B);
  createHostMatrix(c, M_A, N_B);
  fill_Mat(a, M_A, NM_AB);
  fill_Mat(b, NM_AB, N_B);
  fill_Mat(c, M_A, N_B);
  //
  // DEVICE MAT MULTIPLICACION
  createDeviceMatrix(m_1, m_d1, m_dd1, totalsizeA, M_A, NM_AB);
  createDeviceMatrix(m_2, m_d2, m_dd2, totalsizeB, NM_AB, N_B );
  createDeviceMatrix(m_3, m_d3, m_dd3, totalsizeC, M_A, N_B);
  fill_Mat(m_1, M_A, NM_AB);
  fill_Mat(m_2, NM_AB, N_B);
  //fill_Mat(m_3, M, N);

  cudaMemcpy(m_dd1[0], m_1[0], totalsizeA, cudaMemcpyHostToDevice);
  cudaMemcpy(m_dd2[0], m_2[0], totalsizeB, cudaMemcpyHostToDevice);

  // compute time
  cudaEvent_t start, start2;
  cudaEvent_t stop, stop2;

  clock_t t0, t1;
  t0 = clock();
  hostMatMult(a,b,c);
  t1 = clock();
  printf("Multiplicacion Host CPU\t%.16g\n",((t1-t0)/(1.0*CLOCKS_PER_SEC))*1000);



  cudaEventCreate(&start);
  cudaEventCreate(&stop);
  cudaEventRecord(start,0);

  sharedMatMult<<<blocksPerGrid, threadsPerBlock>>>(m_d1, m_d2, m_d3);
  //cudaDeviceSynchronize();

  cudaEventRecord(stop,0);
  cudaEventSynchronize(stop);
  float elapsedTime;
  cudaEventElapsedTime(&elapsedTime, start, stop);
  printf("Multiplicacion Device\t%f\n", elapsedTime );
  cudaEventDestroy(start); cudaEventDestroy(stop);

  cudaMemcpy(m_3[0], m_dd3[0], totalsizeC, cudaMemcpyDeviceToHost);

  printf("\t\tMatriz A\n");
  print_Mat(m_1,M_A,NM_AB);
  printf("\t\tMatriz B\n");
  print_Mat(m_2,NM_AB,N_B);

  printf("\t\tResultado \n");
  print_Mat(m_3,M_A,N_B);

  cudaFree(m_d1);
  cudaFree(m_d2);
  cudaFree(m_d3);







  return 0;
}
