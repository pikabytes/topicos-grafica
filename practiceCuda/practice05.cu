#include <stdio.h>
#include <stdlib.h>


#define N 10
#define M 10
#define THREADS_PER_BLOCK 50

__global__ void add(int **a, int **b, int **c, int n, int m){
	int i = threadIdx.x + (blockIdx.x * blockDim.x);
	int j = threadIdx.y + (blockIdx.y * blockDim.y); 

	if(i < n && j < m)
		c[i][j] = a[i][j] + b[i][j];
}

void random_ints(int *a, int n){
	for(int i=0; i<n; ++i)
		a[i] = rand()%500;
}

void print_matrix(int **mat, int v, int h){
	for(int i=0; i< v; ++i ){
		for(int j=0; j<h; ++j)
			printf("%d ",mat[i][j]);
		printf("\n");
	}
}

void fill_mat(int **a, int v, int h){
	for (int i = 0; i < v; ++i){
		for (int j = 0; i < h; ++j)
			a[i][j] = rand() %500;
	}
}



int main(void){
	int **a, **b, **c;
	int **d_a, **d_b, **d_c;
	//int size = N*M * sizeof(int);
	//int size = M * sizeof(int);

	cudaMalloc((void**)&d_a, sizeof(int)*M*N);
	cudaMalloc((void**)&d_b, size);
	cudaMalloc((void**)&d_c, size);

	a = (int**)malloc(size);	fill_mat(a, N, M);
	b = (int**)malloc(size);	fill_mat(b, N, M);
	c = (int**)malloc(size);

	cudaMalloc(d_a, a, size, cudaMemcpyHostToDevice);
	cudaMalloc(d_b, b, size, cudaMemcpyHostToDevice);

	add<<< 1,1 >>>(d_a, d_b, d_c, N, M);

	cudaMemcpy(c, d_c, size, cudaMemcpyDeviceToHost);
	print_matrix(a, N, M);
	print_matrix(b, N, M);
	print_matrix(c, N, M);

	free(a);	free(b);	free(c);
	cudaFree(d_a);	cudaFree(d_b);	cudaFree(d_c);



	return 0;

}