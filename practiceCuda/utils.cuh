    #include <stdlib.h>
    #include <stdio.h>
    #include "CImg.h"



    using namespace std;
    using namespace cimg_library;



    __host__ __device__ void print_matrix(float **matrix, int m, int n){
        printf("\n");
        for(int i=0; i<m; ++i){
            for(int j=0; j<n; ++j)
                printf("%.2f\t", matrix[i][j]);
            printf("\n");
        }
    }

    void createHostMatrix(float **&matrix, int m, int n){
        matrix = (float**)malloc(m*sizeof(float*));
        for(int i=0; i<m; ++i){
            matrix[i] = (float*)malloc(n*sizeof(float));
        }
    }

    void fill_matrix(float **&matrix, int m, int n){
          for (size_t i = 0; i < m; i++)
            for (size_t j = 0; j < n; j++) {
              matrix[i][j] = 2;//rand()%2+1;
          }
    }


    void createDeviceMatrix(float **&hmatrix, float **&dmatrix, float **&tmatrix, int m, int n){
        size_t row_size = m*sizeof(float*);
        size_t total_size = m*n*sizeof(float);
        //host
        hmatrix = (float**)malloc(row_size);
        hmatrix[0] = (float*)malloc(total_size);
        //device
        cudaMalloc((void**)&dmatrix, row_size);
        //temp
        tmatrix = (float**)malloc(row_size);
        cudaMalloc((void**)&tmatrix[0], total_size);

        for(int i=1; i<m; ++i){
            hmatrix[i] = hmatrix[i-1] +n;
            tmatrix[i] = tmatrix[i-1] +n;
        }
        cudaMemcpy(dmatrix, tmatrix, row_size, cudaMemcpyHostToDevice);
    }

    void createMatrixs(float **&matrix_h, float **&matrix_d, float **&matrix_dd, int m, int n){
          size_t row_size = sizeof(float*)*m;
          size_t totalsize = sizeof(float)*n*m;
          // host
          matrix_h = (float**)malloc(row_size);
          matrix_h[0] = (float*)malloc(totalsize);
          //device
          cudaMalloc((void**)&matrix_d, row_size);
          //temp
          matrix_dd = (float**)malloc(row_size);
          cudaMalloc((void**)&matrix_dd[0],totalsize);

          for (size_t i = 1; i < m; i++) {
            matrix_h[i] = matrix_h[i-1] + n;
            matrix_dd[i] = matrix_dd[i-1] + n;
          }
          cudaMemcpy(matrix_d, matrix_dd, row_size, cudaMemcpyHostToDevice);
        }


    void imageToMatrixs(CImg<unsigned char> imagen, float **&rmatrix, float **&gmatrix, float **&bmatrix){
        for(int i=0; i< imagen.height(); ++i)
            for(int j=0; j<imagen.width(); ++j){
                rmatrix[i][j] = (int)imagen(i,j,0,0);
                gmatrix[i][j] = (int)imagen(i,j,0,1);
                bmatrix[i][j] = (int)imagen(i,j,0,2);
            }
    }


    void setexample(float **&matrix){
        matrix[0][0]=88;  matrix[0][1]=88;  matrix[0][2]=89;  matrix[0][3]=90;  matrix[0][4]=92;  matrix[0][5]=94;  matrix[0][6]=96;  matrix[0][7]=97;
        matrix[1][0]=90;  matrix[1][1]=90;  matrix[1][2]=91;  matrix[1][3]=92;  matrix[1][4]=93;  matrix[1][5]=95;  matrix[1][6]=97;  matrix[1][7]=97;
        matrix[2][0]=92;  matrix[2][1]=92;  matrix[2][2]=93;  matrix[2][3]=94;  matrix[2][4]=95;  matrix[2][5]=96;  matrix[2][6]=97;  matrix[2][7]=97;
        matrix[3][0]=93;  matrix[3][1]=93;  matrix[3][2]=94;  matrix[3][3]=95;  matrix[3][4]=96;  matrix[3][5]=96;  matrix[3][6]=96;  matrix[3][7]=96;
        matrix[4][0]=92;  matrix[4][1]=93;  matrix[4][2]=95;  matrix[4][3]=96;  matrix[4][4]=96;  matrix[4][5]=96;  matrix[4][6]=96;  matrix[4][7]=95;
        matrix[5][0]=92;  matrix[5][1]=94;  matrix[5][2]=96;  matrix[5][3]=98;  matrix[5][4]=99;  matrix[5][5]=99;  matrix[5][6]=98;  matrix[5][7]=97;
        matrix[6][0]=94;  matrix[6][1]=96;  matrix[6][2]=99;  matrix[6][3]=101;  matrix[6][4]=103;  matrix[6][5]=103;  matrix[6][6]=102;  matrix[6][7]=101;
        matrix[7][0]=95;  matrix[7][1]=97;  matrix[7][2]=101;  matrix[7][3]=104;  matrix[7][4]=106;  matrix[7][5]=106;  matrix[7][6]=105;  matrix[7][7]=105;
    }
    void set8matrix(float **&matrix){
        matrix[0][0]=1/8.0;  matrix[0][1]=1/8.0;  matrix[0][2]=1/4.0;  matrix[0][3]=0;  matrix[0][4]=1/2.0;  matrix[0][5]=0;  matrix[0][6]=0;  matrix[0][7]=0;
        matrix[1][0]=1/8.0;  matrix[1][1]=1/8.0;  matrix[1][2]=1/4.0;  matrix[1][3]=0;  matrix[1][4]=-1/2.0;  matrix[1][5]=0;  matrix[1][6]=0;  matrix[1][7]=0;
        matrix[2][0]=1/8.0;  matrix[2][1]=1/8.0;  matrix[2][2]=-1/4.0;  matrix[2][3]=0;  matrix[2][4]=0;  matrix[2][5]=1/2.0;  matrix[2][6]=0;  matrix[2][7]=0;
        matrix[3][0]=1/8.0;  matrix[3][1]=1/8.0;  matrix[3][2]=-1/4.0;  matrix[3][3]=0;  matrix[3][4]=0;  matrix[3][5]=-1/2.0;  matrix[3][6]=0;  matrix[3][7]=0;
        matrix[4][0]=1/8.0;  matrix[4][1]=-1/8.0;  matrix[4][2]=0;  matrix[4][3]=1/4.0;  matrix[4][4]=0;  matrix[4][5]=0;  matrix[4][6]=1/2.0;  matrix[4][7]=0;
        matrix[5][0]=1/8.0;  matrix[5][1]=-1/8.0;  matrix[5][2]=0;  matrix[5][3]=1/4.0;  matrix[5][4]=0;  matrix[5][5]=0;  matrix[5][6]=-1/2.0;  matrix[5][7]=0;
        matrix[6][0]=1/8.0;  matrix[6][1]=-1/8.0;  matrix[6][2]=0;  matrix[6][3]=-1/4.0;  matrix[6][4]=0;  matrix[6][5]=0;  matrix[6][6]=0;  matrix[6][7]=1/2.0;
        matrix[7][0]=1/8.0;  matrix[7][1]=-1/8.0;  matrix[7][2]=0;  matrix[7][3]=-1/4.0;  matrix[7][4]=0;  matrix[7][5]=0;  matrix[7][6]=0;  matrix[7][7]=-1/2.0;
    }

    __device__ __host__ void multMatrixRow(float **&amatrix, float **&filter, float **&cmatrix,
                    int mi, int mf, int ni, int nf, int nfilter){
        for(int i =0; i<(mf-mi); ++i){
            for(int j=0; j<nfilter; ++j){
                float sum =0;
                for(int k=0; k<(nf-ni); ++k){
                    sum += (amatrix[mi+i][ni+k]*filter[k][j]);
                    //printf("%f\t%f\t%f\n",amatrix[mi+i][ni+k],filter[k][j], sum);
                }
                //printf("casaaaa %d\t%d\t%f\n",i+mi, j+ni, sum);
                cmatrix[i+mi][j+ni] = sum;
                //printf("casa %f\n",cmatrix[i+mi][j+ni]);
                //cmatrix[j+ni][i+mi] = 2;//sum;
            }
        }
    }
    __device__ __host__ void multMatrixCol(float **&amatrix, float **&filter, float **&cmatrix,
                    int mi, int mf, int ni, int nf, int mfilter){
        for(int j =0; j<(nf-ni); ++j){
            for(int i=0; i<mfilter; ++i){
                float sum =0;
                for(int k=0; k<(mf-mi); ++k){
                    sum += (amatrix[mi+k][ni+j]*filter[k][i]);
                    printf("%d\t%d\t%f\n",k,j,filter[k][i]);
                }
                printf("casaaaa %d\t%d\t\n",i+mi, j+ni);
                cmatrix[i+mi][j+ni] = sum;
                //printf("casa %f\n",cmatrix[i+mi][j+ni]);
                //cmatrix[j+ni][i+mi] = 2;//sum;
            }
        }
    }


