#include <stdio.h>
#include <stdlib.h>

#define N 4
#define M 4

__global__ void matAdd(double **m_a, double **m_b, double **m_c, int n, int m){
	int row = threadIdx.x + blockIdx.x*blockDim.x;
	int col = threadIdx.y + blockIdx.y*blockDim.y;
	printf("casa\n");
	
	if(row < m && col < n){
		m_c[row][col] = m_a[row][col] + m_b[row][col];
	}
	
}

__global__ void matMul(double **m_a, double **m_b, double **m_c, int n, int m){
	int row = threadIdx.x + blockIdx.x*blockDim.x;
	int col = threadIdx.y + blockIdx.y*blockDim.y;

	
	double sumTmp = 0;
	if( row < m && col < n){
		for(int e = 0; e < m; ++e)
			sumTmp += m_a[row][e] * m_b[e][col];
		m_c[row][col] =  sumTmp;
	}
}


void fill_fakeMatrix(double **&d_a, int n, int m){
	for(int i=0; i<n*m; ++i)		
		d_a[0][i] = rand()%4+1;
}


void create_matrix_host_cuda(double **&h_m, double **&d_m, double **&dd_m, 
								size_t totalsize, int n, int m){

	size_t size_row = sizeof(double*)*n;
	//matrix d_h
	h_m = (double**)malloc(size_row);
	h_m[0] = (double*)malloc(totalsize);
	//matrix d_m 
	cudaMalloc((void**)&d_m, size_row);	
	//matrix dd_m
	dd_m = (double**)malloc(size_row);
	cudaMalloc((void**)&dd_m[0], totalsize);

	//d_m[0] = dd_m[0];
	for(int i=1; i<n; i++){
		h_m[i] =  h_m[i-1]+m; 
		dd_m[i] = dd_m[i-1]+m;
	}
	cudaMemcpy(d_m, dd_m, size_row, cudaMemcpyHostToDevice);
}



void fill_Mat(double **a, int n, int m){
	for (int i = 0; i < (n*m); ++i){
		a[0][i] = rand()%10;
	}
}

void fill_Zero(double **a, int n, int m){
	for(int i=0; i<n; ++i)
		for(int j=0; j<m; ++j)
			a[i][j] = 0;
}

void print_Mat(double **a, int n, int m){
	printf("\n\n");
	for (int i = 0; i < n; ++i){
		for (int j = 0; j < m; ++j){
			printf("%.1f\t", a[i][j]);
		}
		printf("\n");
	}
}


int** read_Mat(int n, int m){
	int **a = (int**)malloc(sizeof(int*)*n);
	for (int i = 0; i < n; ++i)
		a[i] = (int*)malloc(sizeof(int)*m);
	return a;
}

int main(){

	dim3 threads_per_block(N,M);
	dim3 blocks_per_gridx;

	dim3 bloque(N, M);
	dim3 grid( (N+bloque.x -1)/bloque.x, 
				(M+bloque.y-1)/bloque.y );

	// (N + threads_per_block.x - 1)/(threads_per_block.x);
	// (M + threads_per_block.y - 1)/(threads_per_block.y);

	size_t totalsize = sizeof(double)*N*M;
	double **m_1, **m_d1, **m_dd1;
	double **m_2, **m_d2, **m_dd2;
	double **m_3, **m_d3, **m_dd3;

	create_matrix_host_cuda(m_1, m_d1, m_dd1, totalsize, N, M);
	create_matrix_host_cuda(m_2, m_d2, m_dd2, totalsize, N, M);
	create_matrix_host_cuda(m_3, m_d3, m_dd3, totalsize, N, M);

	fill_fakeMatrix(m_1,N,M);
	fill_fakeMatrix(m_2,N,M);
	//fill_fakeMatrix(m_3,N,M);

	cudaMemcpy(m_dd1[0], m_1[0] , totalsize, cudaMemcpyHostToDevice);
	//cudaMemcpy(m_d1, m_dd1, totalsize, cudaMemcpyHostToDevice);
	cudaMemcpy(m_dd2[0], m_2[0], totalsize, cudaMemcpyHostToDevice);
	//cudaMemcpy(m_d2, m_dd2, totalsize, cudaMemcpyHostToDevice);

	print_Mat(m_1, N,M);
	print_Mat(m_2,N,M);

	matAdd<<<grid, bloque>>>(m_d1, m_d2, m_d3, M, N);
	//matMul<<<grid, bloque>>>(m_d1, m_d2, m_d3, M, N);
	cudaDeviceSynchronize();

	cudaMemcpy(m_3[0], m_dd3[0], totalsize, cudaMemcpyDeviceToHost);
	print_Mat(m_3, N, M);


	
	//fill_fakeMatrix(h_m, N, M);
	//print_Mat(m_1, N, M);
	cudaFree(m_d1);
	cudaFree(m_d2);
	cudaFree(m_d3);


	return 0;
}
