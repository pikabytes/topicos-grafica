#include <stdlib.h>
#include <stdio.h>

#define N 5
__global__ void hello(){ printf("hola mundo\n");}

__global__ void add(float *a, float *b, float *c){
	printf("casa\n");
	c[threadIdx.x] = a[threadIdx.x] + b[threadIdx.x];
}

__global__ void add2(float *a, float *b, float *c, int n){
	int i = blockDim.x * blockIdx.x + threadIdx.x;
	//printf("holaaa\n");
	//printf("%d\n", blockDim.x + threadIdx.x); //10 11 12 ... 19
	//printf("%d\n", blockIdx.x + threadIdx.x); // 0 1 2 ... 9
	//printf("%d\n", blockIdx.x * blockDim.x + threadIdx.x);
	printf(" blockDim\t%d\tblockId.X\t%d\tblockId.Y\t%d\tthreadId\t%d\n ", blockDim.x, blockIdx.x, blockIdx.y, threadIdx.x);

	if(i < n)
		c[i] = a[i] + b[i];
}


void random_ints(float * a, float n){
	for(int i=0; i<n; ++i)
		a[i]= rand() % 100+1;
}

void print_ints(float *a, int n){
	for(int i=0; i<n; ++i)
		printf("%f ", a[i]);
	printf("\n");
}

int main(void){
	float *a, *b, *c;
	float *d_a, *d_b, *d_c;
	int  size = N*sizeof(int);


	dim3 dimGrid(3,3,1);
	dim3 dimBlock(2,1,1);


	cudaMalloc((void**)&d_a, size);
	cudaMalloc((void**)&d_b, size);
	cudaMalloc((void**)&d_c, size);

	a = (float*) malloc(size);	random_ints(a,N);
	b = (float*) malloc(size);	random_ints(b,N);
	c = (float*) malloc(size);	

	cudaMemcpy(d_a, a, size, cudaMemcpyHostToDevice);
	cudaMemcpy(d_b, b, size, cudaMemcpyHostToDevice);
	
	add2<<<dimGrid,dimBlock>>>(d_a, d_b, d_c, N);
	//add<<<1,N>>>(d_a, d_b, d_c);
	//hello<<<1,N>>>();
	cudaDeviceSynchronize();
	
	cudaMemcpy(c, d_c, size, cudaMemcpyDeviceToHost);
	
	//print_ints(a,N);
	//print_ints(b,N);
	//print_ints(c,N);
	

	free(a);	free(b);	free(c);
	cudaFree(d_a);	cudaFree(d_b);	cudaFree(d_c);
	


	return 0;
}
