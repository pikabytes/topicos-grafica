#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <stdio.h>
#define ROW 100//filas Matriz 1
#define ColRow 100// columna - fila Matriz 1 y 2
#define COL 100 // columna Matriz 2

#define N1 ROW*ColRow // Cantidad de elementos en Matriz 1
#define N2 ColRow*COL // Cantidad de elementos en Matriz 2
#define N3 ROW*COL // Cantidad de elementos en Matriz 3

#define THREADS 16
using namespace std;

void createMatrixHost(float**& host, int row, int col, int size){
    host = (float **)malloc(row*sizeof(float*));
    host[0]=(float *)malloc(size);

    for (int i=1; i<row;++i){
        host[i]=host[i-1]+col;
    }
}

void createMatrixHostCUDA(float**& host, float**& device, float **& aux, int row, int col, int size){
    host = (float **)malloc(row*sizeof(float*));
    host[0]=(float *)malloc(size);
    aux =(float **)malloc(row*sizeof(float*));

    cudaMalloc((void **)&aux[0],size);
    cudaMalloc((void **)&device,row*sizeof(float*));

    for (int i=1; i<row;++i){
        host[i]=host[i-1]+col;
        aux[i]=aux[i-1]+col;
    }
    cudaMemcpy(device, aux, row*sizeof(float*), cudaMemcpyHostToDevice);
}

void Multiplicacion(float** A, float** B, float** P){
        for(int i=0;i<ROW;i++){
                for(int j=0;j<COL;j++){
                        float Sum=0.0;
                        for(int k=0;k<ColRow;k++){
                                Sum += A[i][k]*B[k][j];
                        }
                        P[i][j] = Sum;
                }
        }
}

__global__ void MatrixMulKernel(float** A, float** B, float** P){
        int Row = blockIdx.y*blockDim.y +threadIdx.y;
        int Col = blockIdx.x*blockDim.x +threadIdx.x;

        if((Row < ROW) && (Col < COL)){
                float Pvalue = 0.0;
                for(int k=0;k<ColRow;k++){
                        Pvalue+= A[Row][k] * B[k][Col];
                }
                P[Row][Col] = Pvalue;
        }
}

__global__ void MatrixMulKernel2(float** A, float** B, float** P)
{
  __shared__ float A_b[THREADS][THREADS];
  __shared__ float B_b[THREADS][THREADS];
  int Row = blockIdx.y * THREADS + threadIdx.y;
  int Col = blockIdx.x * THREADS + threadIdx.x;
  float sum =0.0;

	for(int i = 0;i < ceil(ColRow/(float)THREADS);i++){
		if ((Row<ROW) && (i*THREADS + threadIdx.x<ColRow))
      A_b[threadIdx.y][threadIdx.x] = A[Row] [i*THREADS + threadIdx.x];
		else
      A_b[threadIdx.y][threadIdx.x] = 0.0;

		if ((i*THREADS + threadIdx.y<ColRow) && (Col<COL))
      B_b[threadIdx.y][threadIdx.x] = B[i*THREADS + threadIdx.y][Col];
		else
      B_b[threadIdx.y][threadIdx.x] = 0.0;
    __syncthreads();

    for (int k = 0; k < THREADS; k++) {
            sum += A_b[threadIdx.y][k] * B_b[k][threadIdx.x];
            //printf("%f\t%f\n", A_b[threadIdx.y][k], B_b[k][threadIdx.x]);
    }
    __syncthreads();
	 }
	 if((Row<ROW) && (Col<COL))
	         P[Row][Col] = sum;
}

void llenarVector(float **V, int row, int col){
    for(int i=0;i<row;i++){
	for(int j=0;j<col;j++){
	        V[i][j]=rand()%10+1;
	}
    }
}

void imprimir(float **M, int row, int col){
        for(int i=0;i<row;i++){
                for(int j=0;j<col;j++){
                        printf("%f\t", M[i][j] );
                }
                cout<<endl;
        }
        cout<<endl;
}

int main(){
	float **a, **b, **c1,**c2,**c3;
	//////////////////////////////////////////
	float **d_a, **d_b, **d_c2, **d_c3;
	float **a_aux, **b_aux, **c_aux2, **c_aux3;
	///////////////////////////////////////////

	int size1 = N1 * sizeof(float*);
	int size2 = N2 * sizeof(float*);
	int size3 = N3 * sizeof(float*);

	dim3 DimGrid(((COL-1)/THREADS)+1, ((ROW-1)/THREADS)+1, 1);
  dim3 DimBlock(THREADS, THREADS, 1);

	cout<<ceil(ColRow/(float)THREADS)<<endl;
	cout<<((COL-1)/THREADS)+1<<endl;
	cout<<((ROW-1)/THREADS)+1<<endl;

	createMatrixHost(c1,ROW,COL,size3);
	createMatrixHostCUDA(a,d_a,a_aux,ROW,ColRow,size1);
	createMatrixHostCUDA(b,d_b,b_aux,ColRow,COL,size2);

	createMatrixHostCUDA(c2,d_c2,c_aux2,ROW,COL,size3);
	createMatrixHostCUDA(c3,d_c3,c_aux3,ROW,COL,size3);

  llenarVector(a,ROW,ColRow);
	llenarVector(b,ColRow,COL);

	//imprimir(a,ROW,ColRow);
	//imprimir(b,ColRow,COL);

	cudaMemcpy(a_aux[0], a[0], size1, cudaMemcpyHostToDevice);
	cudaMemcpy(b_aux[0], b[0], size2, cudaMemcpyHostToDevice);
/**********************************************************************************/
	cudaEvent_t start,start2;
  cudaEvent_t stop,stop2;

	//clock_t t0, t1;
/**********************************************************************************/
/*
  t0=clock();
	Multiplicacion(a,b,c1);
	//imprimir(c1,ROW,COL);
	t1=clock();
	double secs;
	secs = (double)(t1 - t0) / CLOCKS_PER_SEC;
	printf("Mat Multiplicacion CPU  %.16g milisegundos\n", secs * 1000.0);
*/
/**********************************************************************************/
	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	cudaEventRecord(start,0);
	MatrixMulKernel<<<DimGrid,DimBlock>>>(d_a,d_b,d_c2);
	cudaEventRecord(stop,0);
  cudaEventSynchronize(stop);
	float elapsedTime;



  cudaEventElapsedTime(&elapsedTime,start,stop);
  cout<<"Mat Multiplicacion GPU: "<<elapsedTime<<endl;
  cudaEventDestroy(start);
  cudaEventDestroy(stop);

	cudaMemcpy(c2[0],c_aux2[0], size3, cudaMemcpyDeviceToHost);
	//imprimir(c2,ROW,COL);
/************************************************************************************/
	cudaEventCreate(&start2);
  cudaEventCreate(&stop2);
  cudaEventRecord(start2,0);
	MatrixMulKernel2<<<DimGrid,DimBlock>>>(d_a,d_b,d_c3);
	cudaEventRecord(stop2,0);
  cudaEventSynchronize(stop2);
  float elapsedTime2;
  cudaEventElapsedTime(&elapsedTime2,start2,stop2);
  cout<<"Mat Shared Multiplicacion GPU: "<<elapsedTime2<<endl;
  cudaEventDestroy(start2);
  cudaEventDestroy(stop2);
	cudaMemcpy(c3[0],c_aux3[0], size3, cudaMemcpyDeviceToHost);
	//imprimir(c3,ROW,COL);
}
