#include <stdio.h>
#include <stdlib.h>


#define M_mat 4
#define N_mat 4
#define BLOCK_SIZE 3
#define RADIUS 1
#define MN_filter 3


void fill_Mat(double **&matrix, int m, int n){
  for(int i=0; i<m; ++i)
    for(int j=0; j<n; ++j){
      matrix[i][j] = rand()%10+1;
    }
}

void print_Mat(double **matrix, int m, int n){
  for(int i=0; i<m; ++i){
    for(int j=0; j<n; j++)
      printf("%3.f\t", matrix[i][j]);
    printf("\n");
  }
}



void createMatrix(double **&h_m, double **&d_m, double **&dd_m,
                  size_t totalsize, int M,int N){
  size_t size_row = sizeof(double*)*M;
  //size_t totalsize = sizeof(double)*M*N;
  //matrix D_H host matrix
  h_m = (double**)malloc(size_row);
  h_m[0] = (double*)malloc(totalsize);
  //matrix D_M device matrix
  cudaMalloc((void**)&d_m, size_row);
  //matrix DD_M
  dd_m = (double**)malloc(size_row);
  cudaMalloc((void**)&dd_m[0], totalsize);
  //linkear las filas a sus respectivas columnas
  for(int i=1; i<M; ++i){
    h_m[i] = h_m[i-1] + N;
    dd_m[i] = dd_m[i-1] + N;
  }
  cudaMemcpy(d_m, dd_m, size_row, cudaMemcpyHostToDevice);
}


__global__ void convolucion(int **m_in, int **m_f, int **m_out){


}


int main(int argc, char const *argv[]) {
dim3 threadsPerBlock(BLOCK_SIZE, BLOCK_SIZE);
dim3 blocksPerGrid((M-1)/threadsPerBlock.x+1,
                    (N-1)/threadsPerBlock.y+1);
size_t totalsize = sizeof(double)*M*N;
size_t filtersize = sizeof(double)*MN_filter*MN_filter;
double **m_1,**m_d1,**m_dd1; //matrix in
double **m_2,**m_d2,**m_dd2; //filtro
double **m_3,**m_d3,**m_dd3; //matrix out
createMatrix(m_1, m_d1, m_dd1, M, N);
fill_Mat(m_1, M, N);

cudaMemcpy(m_dd1[0], m_1[0], totalsize, cudaMemcpyHostToDevice);
cudaMemcpy(m_dd2[0], m_2[0], filtersize, cudaMemcpyHostToDevice);
convolucion<<< blocksPerGrid, threadsPerBlock>>>(m_d1, m_d2);
cudaDeviceSynchronize();

cudaMemcpy(m_3[0], m_dd3[0], totalsize, cudaMemcpyHostToDevice);



  return 0;
}
