#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QString>




MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionClose_triggered()
{
    close();
}

void MainWindow::on_actionOpen_triggered()
{
    QString filename = QFileDialog::getOpenFileName(
                this,
                tr("Open BMP Files"),
                "*.bmp"
                );

    cout<<filename.toStdString()<<endl;

}

void MainWindow::on_pushButton_clicked()
{
    QString imageA = QFileDialog::getOpenFileName(
                this,
                tr("Open Image A"),
                "*.bmp"
                );


    QImage imgA = QImage(imageA);
    QGraphicsScene *graphic = new QGraphicsScene(this);
    graphic->addPixmap(QPixmap::fromImage(imgA));
    ui->inputAgraphicsView->setScene(graphic);

}

void MainWindow::on_pushButton_2_clicked()
{
    QString imageB = QFileDialog::getOpenFileName(
                this,
                tr("Open Image B"),
                "*.bmp"
                );

    QImage imgB = QImage(imageB);
    QGraphicsScene *graphic = new QGraphicsScene(this);
    graphic->addPixmap(QPixmap::fromImage(imgB));
    ui->inputBgraphicsView->setScene(graphic);
}
