#include "mainwindow.h"
#include <QApplication>
#include <iostream>
#include <opencv2/highgui.hpp>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>

using namespace std;
using namespace cv;

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    /*
    Mat image;
    image = imread("lena512.bmp");
    if(!image.data){
        cout<<"no hay imagen"<<endl;
    }else{
        cout<<"imagen correcta"<<endl;
    }

    cv::namedWindow("imagen");
    cv::imshow("imagen",image);
    cv::waitKey(0);
    */


    MainWindow w;
    w.show();
    return a.exec();
}
